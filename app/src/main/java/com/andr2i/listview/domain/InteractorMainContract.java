package com.andr2i.listview.domain;

import com.andr2i.listview.data.model.db.User;
import java.util.List;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;


public interface InteractorMainContract {

    Completable insertUser(User user);

    Single<List<User>> readContact();

    Completable updateUser(User user);

    Completable deleteUser(User user);

}
