package com.andr2i.listview.domain;

import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.data.repository.RepositoryContact;
import com.andr2i.listview.domain.base.BaseInteractor;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;


public class InteractorMain extends BaseInteractor implements InteractorMainContract {


    public InteractorMain() {
    }

    @Override
    public Single<List<User>> readContact() {
        return RepositoryContact.getInstance().getAll()
                .flatMapIterable(contacts -> contacts)
                .map(user -> {
                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    if (!user.email.isEmpty())
                        user.emailQR = barcodeEncoder.encodeBitmap(user.email, BarcodeFormat.QR_CODE, 200, 200);
                    // user.email = user.email;
                    return user;
                })
                .toList()
                .compose(applySingleSchedulers());
    }


    public Completable insertUser(final User user) {
        return Completable.fromAction(() -> RepositoryContact.getInstance().create(user))
                .compose(applyCompletableSchedulers());
    }

    @Override
    public Completable deleteUser(User user) {
        return Completable.fromAction(() -> RepositoryContact.getInstance().delete(user))
                .compose(applyCompletableSchedulers());
    }






    @Override
    public Completable updateUser(User user) {
        return Completable.fromAction(() -> RepositoryContact.getInstance().update(user))
                .compose(applyCompletableSchedulers());

    }



}