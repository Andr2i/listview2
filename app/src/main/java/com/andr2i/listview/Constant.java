package com.andr2i.listview;

public abstract class Constant {

    public static final String DIALOG_UPDATE = "dialog_update";
    public static final String MAIN_FRAGMENT = "main_fragment";
    public static final String CONTACTLISTFRAGMENT = "ContactListFragment";
    public static final String CONTACTEDITFRAGMENT = "ContactEditFragment";
    public static final String CONTACTCARDFRAGMENT = "ContactCardFragment";

    public static final String NAME_PREF_STORAGE = "businesscard";


    public static final String DB_NAME = "db_user.db" ;
}
