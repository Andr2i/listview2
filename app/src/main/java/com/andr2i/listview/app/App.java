package com.andr2i.listview.app;

import android.app.Application;

import com.andr2i.listview.data.repository.RepositoryContact;
import com.andr2i.listview.presentation.Route;
import timber.log.Timber;

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        Route.getInstance();
        RepositoryContact.getInstance().initDb(this);

    }



}
