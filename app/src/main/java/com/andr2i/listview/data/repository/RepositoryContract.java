package com.andr2i.listview.data.repository;


import java.util.List;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface RepositoryContract<T> {

    Single<T> getById(int id);

    void create(T entity);

    void delete(T entity);

    void update(T entity);

    Observable<List<T>> getAll();

}