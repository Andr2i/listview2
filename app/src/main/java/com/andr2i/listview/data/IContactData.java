//package com.andr2i.listview.data;
//
//
//import com.andr2i.listview.data.model.db.User;
//
//import java.util.List;
//
//import io.reactivex.Single;
//
//public interface IContactData {
//
//    Single<User> createContact(int id);
//
//    Single<List<User>> readContact();
//
//    Single<User> updateContact(User user);
//
//    Single<Boolean> deleteContact(User user);
//}
