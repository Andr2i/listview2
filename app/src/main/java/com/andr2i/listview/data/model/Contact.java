//package com.andr2i.listview.data.model;
//
//import android.graphics.Bitmap;
//import android.os.Parcel;
//import android.os.Parcelable;
//
//public class Contact extends AbstractEntity implements Parcelable {
//
//    public int id;
//    public String name;
//    public String address;
//    public String phone;
//    public String email;
//    public Bitmap emailQR;
//
//    public Contact(int id, String name, String address, String phone,String email) {
//        this.id = id;
//        this.name = name;
//        this.address = address;
//        this.phone = phone;
//        this.email = email;
//    }
//
//    public Contact(int id) {
//        this.id = id;
//        this.name = "";
//        this.address = "";
//        this.email = "";
//    }
//
//
//    protected Contact(Parcel in) {
//        id = in.readInt();
//        name = in.readString();
//        address = in.readString();
//        phone = in.readString();
//        email = in.readString();
//    }
//
//    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
//        @Override
//        public Contact createFromParcel(Parcel in) {
//            return new Contact(in);
//        }
//
//        @Override
//        public Contact[] newArray(int size) {
//            return new Contact[size];
//        }
//    };
//
//    public int getId() {
//        return id;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (o == null || getClass() != o.getClass()) {
//            return false;
//        }
//        Contact contact = (Contact) o;
//        return id == contact.id;
//    }
//
//    @Override
//    public int hashCode() {
//        return id;
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeInt(id);
//        parcel.writeString(name);
//        parcel.writeString(address);
//        parcel.writeString(phone);
//        parcel.writeString(email);
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//}
