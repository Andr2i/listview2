package com.andr2i.listview.data.repository;


import android.content.Context;

import com.andr2i.listview.data.db.AppDatabase;
import com.andr2i.listview.data.model.db.User;
import java.util.List;
import java.util.concurrent.Callable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class RepositoryContact implements RepositoryContract<User> {

    private static RepositoryContact instance;

    AppDatabase db;

    private RepositoryContact() {

    }

    public void  initDb(Context context)
    {
        db = AppDatabase.getDatabase(context);
    }

    public static synchronized RepositoryContact getInstance() {
        if (instance == null) {
            instance = new RepositoryContact();
        }
        return instance;
    }

    @Override
    public Single<User> getById(int id) {
       User user = db.userDao().GetByIds(id);
        return Single.just(user);
    }

    @Override
    public void create(User entity) {
        db.userDao().insert(entity);

    }

    @Override
    public void delete(User entity) {
        db.userDao().delete(entity);

    }

    @Override
    public void update(User entity) {
//        SharedPreferencesStorage.getInstance().putObject(String.valueOf(entity.id), entity);
       // App.getDatabase().userDao().(entity);
    }

    @Override
    public Observable<List<User>> getAll() {

        return Observable.fromCallable(new Callable<List<User>>() {
            @Override
            public List<User> call() throws Exception {
                return db.userDao().loadAll();
            }
        });

    }


}
