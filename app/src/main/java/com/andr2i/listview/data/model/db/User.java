
package com.andr2i.listview.data.model.db;

import android.graphics.Bitmap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.andr2i.listview.data.model.AbstractEntity;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity(tableName = "users")
public class User extends AbstractEntity implements Serializable {

    public User(String name, String address, String phone, String email) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
    }

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    public Long id;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    public String name;

    @ColumnInfo(name = "address")
    @SerializedName("address")
    public String address;

    @ColumnInfo(name = "email")
    @SerializedName("email")
    public String email;

    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    public String phone;

    @Ignore
    public Bitmap emailQR;
}

