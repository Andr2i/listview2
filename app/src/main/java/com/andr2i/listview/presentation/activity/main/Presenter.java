package com.andr2i.listview.presentation.activity.main;


import com.andr2i.listview.Constant;
import com.andr2i.listview.presentation.Route;


public class Presenter implements IPresenter.Listener {
    private IPresenter.View view;

    @Override
    public void startView(IPresenter.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void init() {
        Route.getInstance().transaction(Constant.CONTACTLISTFRAGMENT, null);
    }


}
