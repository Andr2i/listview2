package com.andr2i.listview.presentation;

import com.andr2i.listview.data.model.AbstractEntity;
import com.andr2i.listview.presentation.base.BaseActivityContract;
import com.andr2i.listview.presentation.base.BaseFragment;

public interface IRoute {

    void startView(BaseActivityContract view);

    void stopView();

    void startFragment(BaseFragment fragment);

    void transaction(String cmd, AbstractEntity entity);


}
