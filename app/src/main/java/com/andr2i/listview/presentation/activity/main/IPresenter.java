package com.andr2i.listview.presentation.activity.main;

import com.andr2i.listview.presentation.base.BaseActivityContract;
import com.andr2i.listview.presentation.base.BasePresenter;


public interface IPresenter {
    interface View extends BaseActivityContract {

    }

    interface Listener extends BasePresenter<View> {
        void init();
    }
}
