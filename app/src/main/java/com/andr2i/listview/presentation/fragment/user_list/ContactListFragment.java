package com.andr2i.listview.presentation.fragment.user_list;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andr2i.listview.R;
import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.databinding.RecyclerViewBinding;
import com.andr2i.listview.presentation.base.BaseFragment;
import com.andr2i.listview.presentation.base.BasePresenter;

import java.util.List;


public class ContactListFragment extends BaseFragment<RecyclerViewBinding> implements ListContactContract.View {

    private ListContactContract.Presenter presenter;
    private static final String TAG_DATA = "data";
    private RecyclerView recyclerView;
    private ContactListAdapter contactsAdapter;

    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    public ContactListFragment() {

    }


    @Override
    protected int getLayoutRes() {
        return R.layout.recycler_view;
    }

    @Override
    protected void initFragmentView() {

        presenter = new ContactListPresenter();
        // getBinding().setEvent(presenter);
        presenter.startView(this);
        presenter.init();

    }

    @Override
    protected void startFragment() {

    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {

    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }


    @Override
    public void initAdapter(List<User> list) {
        contactsAdapter = new ContactListAdapter(super.getContext(), this);
        recyclerView = (RecyclerView) getView().findViewById(R.id.contact_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(contactsAdapter);
        contactsAdapter.setItems(list);

    }

    @Override
    public void onShowContactCardClick(int position) {
        User user = contactsAdapter.getItem(position);
        if (user != null)
            presenter.showContactCard(user);
    }

    @Override
    public void onUpdateClick(int position) {
        User user = contactsAdapter.getItem(position);
        presenter.editContact(user);
    }

    @Override
    public void onDeleteClick(int position) {
        User user = contactsAdapter.getItem(position);
        presenter.deleteContact(user);
        contactsAdapter.remove(user);
    }

    @Override
    public String getTitle() {
        return "СПИСОК КОНТАКТОВ";
    }

    @Override
    public int getFabButtonIcon() {
        return android.R.drawable.ic_input_add;
    }

    @Override
    public View.OnClickListener getFabButtonAction() {
        return v -> presenter.addContact();
    }


}
