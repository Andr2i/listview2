package com.andr2i.listview.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.andr2i.listview.presentation.Route;

import java.util.Objects;


public abstract class BaseFragment<Binding extends ViewDataBinding> extends Fragment {
    private Binding binding;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStop() {
        stopFragment();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onPause() {
        pauseFragment();
        super.onPause();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initFragmentView();
        Route.getInstance().startFragment(this);

        return binding.getRoot();
    }

    protected void toastShort(String message){
        Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();
    }

    protected void toastLong(String message){
        Toast.makeText(getActivity(),message, Toast.LENGTH_LONG).show();
    }

    protected Binding getBinding() {
        return binding;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void initFragmentView();

    protected abstract void startFragment();

    protected abstract void stopFragment();

    protected abstract void destroyFragment();

    protected abstract void pauseFragment();

    protected abstract BasePresenter getPresenter();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        if (getPresenter() != null) {
            getPresenter().detachView();
        }
        destroyFragment();
        super.onDetach();
    }

    public void hideKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) Objects.requireNonNull(this.getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.getActivity().getWindow().getDecorView().getWindowToken(), 0);

    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(this.getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).showSoftInput(this.getActivity().getWindow().getDecorView(), InputMethodManager.SHOW_IMPLICIT);
    }

    public abstract String getTitle();
    @DrawableRes
    public abstract int getFabButtonIcon();

    public abstract View.OnClickListener getFabButtonAction();
}
