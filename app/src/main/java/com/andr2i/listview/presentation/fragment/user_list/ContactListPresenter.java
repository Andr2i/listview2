package com.andr2i.listview.presentation.fragment.user_list;


import com.andr2i.listview.Constant;
import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.domain.InteractorMain;
import com.andr2i.listview.domain.InteractorMainContract;
import com.andr2i.listview.presentation.Route;

import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class ContactListPresenter implements ListContactContract.Presenter {

    private ListContactContract.View view;
    private InteractorMainContract interactor;


    public ContactListPresenter() {
        interactor = new InteractorMain();
    }

    @Override
    public void startView(ListContactContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void init() {
        interactor.readContact()
                .subscribe(new DisposableSingleObserver<List<User>>() {
                    @Override
                    public void onSuccess(List<User> users) {
                        view.initAdapter(users);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("read error %s", e.getMessage());
                    }
                });
    }

    @Override
    public void addContact() {
        Route.getInstance().transaction(Constant.CONTACTEDITFRAGMENT, new User("", "", "", ""));

    }

    @Override
    public void editContact(User user) {
        Route.getInstance().transaction(Constant.CONTACTEDITFRAGMENT, user);
    }

    @Override
    public void deleteContact(User user) {
        interactor.deleteUser(user);
    }

    @Override
    public void showContactCard(User user) {
        Route.getInstance().transaction(Constant.CONTACTCARDFRAGMENT, user);
    }


}