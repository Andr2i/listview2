package com.andr2i.listview.presentation.fragment.user_card;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.andr2i.listview.R;
import com.andr2i.listview.data.model.AbstractEntity;
import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.databinding.ContactCardBinding;
import com.andr2i.listview.presentation.base.BaseFragment;
import com.andr2i.listview.presentation.base.BasePresenter;


public class ContactCardFragment extends BaseFragment<ContactCardBinding> implements ContactCardContract.View {

    private ContactCardContract.Presenter presenter;
    private User user;
    private static final String CONTACT = "CONTACT";

    public static ContactCardFragment newInstance(AbstractEntity user) {
        ContactCardFragment fragment = new ContactCardFragment();
        Bundle args = new Bundle();
        args.putSerializable(CONTACT, (User) user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.contact_card;
    }

    @Override
    protected void initFragmentView() {
        user = (User) getArguments().getSerializable(CONTACT);
        assert user != null;
        presenter = new ContactCardPresenter();
        getBinding().setUser(user);


    }

    @Override
    protected void startFragment() {
        showQrCode(user.emailQR);
    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {

    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public int getFabButtonIcon() {
        return 0;
    }

    @Override
    public View.OnClickListener getFabButtonAction() {
        return null;
    }


    public void showQrCode(Bitmap bitmap) {
        //  getBinding().setCardQr(bitmap);
        ImageView imV = this.getView().findViewById(R.id.card_generated_img);
        imV.setImageBitmap(bitmap);
    }


}
