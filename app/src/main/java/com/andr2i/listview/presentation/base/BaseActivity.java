package com.andr2i.listview.presentation.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.andr2i.listview.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;


public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {

    private Binding binding;
    Toolbar toolbar;
    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        initView();

        toolbar = findViewById(R.id.toolbar);
        floatingActionButton = findViewById(R.id.fab);
    }

    protected void transactionFragmentNoBackStack(Fragment fragment) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment, fragment.getClass().getSimpleName())
                .commit();
    }


    protected void transactionFragmentWithBackStack(Fragment fragment) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    protected void transactionFragmentDialog(DialogFragment fragment) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content, fragment, fragment.getClass().getSimpleName())
                .commit();
    }

    protected void transactionActivity(Class<?> activity, boolean cycleFinish) {
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            startActivity(intent);
            if (cycleFinish) {
                this.finish();
            }
        }
    }

    protected <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T object) {
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            if (object != null) {

            }
            startActivity(intent);
            if (cycleFinish) {
                this.finish();
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager manager = this.getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 0) {
            super.finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        onStartView();
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding() {
        return binding;
    }

    protected abstract void initView();


    @Override
    public void onDestroy() {
        if (getPresenter() != null) {
            onDestroyView();
            getPresenter().detachView();
        }
        super.onDestroy();
    }

    protected abstract void onStartView();

    protected abstract void onDestroyView();

    protected abstract BasePresenter getPresenter();

    protected void hideKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

    }

    protected void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).showSoftInput(this.getWindow().getDecorView(), InputMethodManager.SHOW_IMPLICIT);
    }

    protected void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    public void resolveFab(BaseFragment fragment) {
        if ((Integer) fragment.getFabButtonIcon() > 0) {
            floatingActionButton.setImageResource(fragment.getFabButtonIcon());
            floatingActionButton.setVisibility(View.VISIBLE);
            floatingActionButton.setOnClickListener(fragment.getFabButtonAction());
        } else {
            floatingActionButton.setVisibility(View.GONE);
            floatingActionButton.setOnClickListener(null);
        }
    }

    public void resolveToolbar(BaseFragment fragment) {
        toolbar.setTitle(fragment.getTitle());
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setNavigationOnClickListener(null);
        }
    }

}
