package com.andr2i.listview.presentation.fragment.usert_edit;


import android.os.Bundle;
import android.view.View;

import com.andr2i.listview.R;
import com.andr2i.listview.data.model.AbstractEntity;
import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.databinding.ContactEtitBinding;
import com.andr2i.listview.presentation.base.BaseFragment;
import com.andr2i.listview.presentation.base.BasePresenter;

public class ContactsEditFragment extends BaseFragment<ContactEtitBinding> implements EditContactContract.View {

    private EditContactsPresenter presenter;
    private User user;
    private static final String TAG = ContactsEditFragment.class.getName();

    public static ContactsEditFragment newInstance(AbstractEntity user) {
        ContactsEditFragment fragment = new ContactsEditFragment();
        Bundle args = new Bundle();
        args.putSerializable(TAG, (User) user);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.contact_etit;
    }

    @Override
    protected void initFragmentView() {
        user = (User) getArguments().getSerializable(TAG);
        assert user != null;
        presenter = new EditContactsPresenter();
        getBinding().setUser(user);
    }

    @Override
    protected void startFragment() {

    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {

    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }



    @Override
    public String getTitle() {
        return user.name;
    }

    @Override
    public int getFabButtonIcon() {
        return android.R.drawable.ic_menu_save;
    }

    @Override
    public View.OnClickListener getFabButtonAction() {
        return v -> saveContacts();
    }

    public void saveContacts() {
        presenter.saveContact(user);
        getActivity().onBackPressed();
    }





}


