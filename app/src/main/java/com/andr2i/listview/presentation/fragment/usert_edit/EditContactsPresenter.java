package com.andr2i.listview.presentation.fragment.usert_edit;

import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.domain.InteractorMain;
import com.andr2i.listview.domain.InteractorMainContract;
import com.andr2i.listview.presentation.fragment.user_list.ListContactContract;

public class EditContactsPresenter implements EditContactContract.Presenter {

    private static final String TAG = "EditContactsPresenter";
    private InteractorMainContract interactor;

    public EditContactsPresenter() {
        interactor = new InteractorMain();
    }

    @Override
    public void startView(ListContactContract.View view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void init() {

    }

    @Override
    public void saveContact(User user) {
        if (user != null)
            if (user.id == null)
            {
                interactor.insertUser(user).subscribe();
            }
            else
                interactor.updateUser(user);
    }


}
