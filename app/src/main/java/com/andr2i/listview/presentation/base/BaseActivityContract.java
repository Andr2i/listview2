package com.andr2i.listview.presentation.base;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public interface BaseActivityContract {

    <T> void setData(T data);

    <T> void transactionActivity(Class<?> activity, T object);

    void transactionFragmentDialog(DialogFragment fragment);

    void transactionActivity(Class<?> activity);

    void transitionFragment(Fragment fragment);

    void transitionFragmentDialog(DialogFragment fragment);

    void resolveFab(BaseFragment fragment);

    void resolveToolbar(BaseFragment fragment);

}
