package com.andr2i.listview.presentation.fragment.user_list;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.andr2i.listview.R;
import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.databinding.ContactItemBinding;
import com.andr2i.utils.recyclerview.BaseViewHolder;
import com.andr2i.utils.recyclerview.GenericRecyclerViewAdapter;


import java.util.Random;

public class ContactListAdapter extends GenericRecyclerViewAdapter<User, ListContactContract.View, ContactListAdapter.ContactListViewHolder> {


    public ContactListAdapter(Context context, ListContactContract.View listener) {
        super(context, listener);
    }

    @Override
    public ContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactListViewHolder(inflate(R.layout.contact_item, parent), getListener());
    }


    /******************************
     * ViewHolder
     ******************************/

    class ContactListViewHolder extends BaseViewHolder<User, ListContactContract.View> {

        private TextView nameTv;
        ContactItemBinding binding;

        public ContactListViewHolder(View itemView, ListContactContract.View listener) {
            super(itemView, listener);
            binding = DataBindingUtil.bind(itemView);
            initViews();
        }

        /**
         * Initialize views and set the listeners
         */

        private void initViews() {

            if (getListener() != null) {
                final ImageButton viewBtn = itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(v -> getListener().onShowContactCardClick(getAdapterPosition()));

                final ImageButton updateBtn = itemView.findViewById(R.id.btn_edit);
                updateBtn.setOnClickListener(v -> getListener().onUpdateClick(getAdapterPosition()));

                final ImageButton deleteeBtn = itemView.findViewById(R.id.btn_delete);
                deleteeBtn.setOnClickListener(v -> getListener().onDeleteClick(getAdapterPosition()));

            }
        }


        @Override
        public void onBind(User item) {
            // bind data to the views
            binding.contactName.setText(item.name);
            binding.contactAddress.setText(item.address);
            binding.contactPhone.setText(item.phone);
            binding.contactEmail.setText(item.email);
            if (item.name.length() > 0)
                binding.tvIcon.setText(item.name.substring(0, 1).toUpperCase());

            Random mRandom = new Random();
            int color = Color.argb(255, mRandom.nextInt(256), mRandom.nextInt(256), mRandom.nextInt(256));
            ((GradientDrawable) binding.tvIcon.getBackground()).setColor(color);

        }
    }


}
