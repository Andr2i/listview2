package com.andr2i.listview.presentation.activity.splash;

import com.andr2i.listview.presentation.base.BaseActivityContract;
import com.andr2i.listview.presentation.base.BasePresenter;

public interface SplashContract {
    interface View extends BaseActivityContract {
        void message(String s);
    }

    interface Presenter extends BasePresenter<View> {
        void pause();
    }
}
