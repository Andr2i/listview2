package com.andr2i.listview.presentation.activity.splash;


import com.andr2i.listview.Constant;
import com.andr2i.listview.presentation.Route;
import com.andr2i.listview.presentation.activity.main.MainActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SplashPresenter implements SplashContract.Presenter {
    private SplashContract.View view;

    private Single<Long> subcribe;


    @Override
    public void startView(SplashContract.View view) {
        this.view = view;
        subcribe.subscribe(new DisposableSingleObserver<Long>() {
            @Override
            public void onSuccess(Long aLong) {
                Route.getInstance().transaction(Constant.MAIN_FRAGMENT, null);
            }

            @Override
            public void onError(Throwable e) {
                if (view != null) view.message("error:)");
            }
        });
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void init() {

    }

    @Override
    public void pause() {
        subcribe = Single.timer(2, TimeUnit.SECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }


}