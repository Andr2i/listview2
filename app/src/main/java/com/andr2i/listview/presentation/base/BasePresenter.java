package com.andr2i.listview.presentation.base;

public interface BasePresenter<V> {

    void startView(V view);

    void detachView();

    void init();
}
