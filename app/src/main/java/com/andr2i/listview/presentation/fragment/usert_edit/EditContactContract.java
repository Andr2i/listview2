package com.andr2i.listview.presentation.fragment.usert_edit;

import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.presentation.base.BaseFragmentContract;
import com.andr2i.listview.presentation.base.BasePresenter;
import com.andr2i.listview.presentation.fragment.user_list.ListContactContract;


public class EditContactContract {

    interface View extends BaseFragmentContract {

    }

    interface Presenter extends BasePresenter<ListContactContract.View> {

        void saveContact(User data);
    }

}
