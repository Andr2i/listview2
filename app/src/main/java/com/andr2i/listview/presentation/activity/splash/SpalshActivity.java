package com.andr2i.listview.presentation.activity.splash;


import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.andr2i.listview.R;
import com.andr2i.listview.databinding.ActivitySpalshBinding;
import com.andr2i.listview.presentation.Route;
import com.andr2i.listview.presentation.base.BaseActivity;
import com.andr2i.listview.presentation.base.BasePresenter;


public class SpalshActivity extends BaseActivity<ActivitySpalshBinding> implements SplashContract.View {
    private SplashContract.Presenter presenter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_spalsh;
    }

    @Override
    protected void initView() {
        presenter = new SplashPresenter();
        Route.getInstance().startView(this);
        presenter.pause();
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void transactionActivity(Class<?> activity, boolean cycleFinish) {
        super.transactionActivity(activity, cycleFinish);
    }


    @Override
    public void message(String val) {
        toast(val);
    }


    @Override
    public <T> void setData(T data) {

    }

    @Override
    public <T> void transactionActivity(Class<?> activity, T object) {

    }

    @Override
    public void transactionFragmentDialog(DialogFragment fragment) {
        super.transactionFragmentDialog(fragment);
    }

    @Override
    public void transactionActivity(Class<?> activity) {
        super.transactionActivity(activity, true);
    }

    @Override
    public void transitionFragment(Fragment fragment) {

    }

    @Override
    public void transitionFragmentDialog(DialogFragment fragment) {

    }


}