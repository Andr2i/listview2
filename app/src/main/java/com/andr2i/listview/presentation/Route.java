package com.andr2i.listview.presentation;

import com.andr2i.listview.Constant;
import com.andr2i.listview.data.model.AbstractEntity;
import com.andr2i.listview.presentation.base.BaseActivityContract;
import com.andr2i.listview.presentation.base.BaseFragment;
import com.andr2i.listview.presentation.fragment.user_card.ContactCardFragment;
import com.andr2i.listview.presentation.fragment.usert_edit.ContactsEditFragment;
import com.andr2i.listview.presentation.fragment.user_list.ContactListFragment;
import com.andr2i.listview.presentation.activity.main.MainActivity;


public class Route implements IRoute {
    private static IRoute instance;
    private BaseActivityContract view;

    private Route() {
    }

    public static synchronized IRoute getInstance() {
        if (instance == null) {
            instance = new Route();
        }
        return instance;
    }

    @Override
    public void startView(BaseActivityContract view) {
        if (view != null) {
            this.view = view;
        }
    }

    @Override
    public void stopView() {
        if (view != null) view = null;
    }

    @Override
    public void startFragment(BaseFragment fragment) {
        if (view != null) {
            view.resolveFab(fragment);
            view.resolveToolbar(fragment);
        }
    }


    @Override
    public void transaction(String cmd, AbstractEntity entity) {
        if (view != null) {
            switch (cmd) {
                case Constant.MAIN_FRAGMENT:
                    view.transactionActivity(MainActivity.class);
                    break;
                case Constant.CONTACTLISTFRAGMENT:
                    view.transitionFragment(ContactListFragment.newInstance());
                    break;
                case Constant.CONTACTEDITFRAGMENT:
                    view.transitionFragment(ContactsEditFragment.newInstance(entity));
                    break;
                case Constant.CONTACTCARDFRAGMENT:
                    view.transitionFragment(ContactCardFragment.newInstance(entity));
                    break;

            }
        }
    }

}
