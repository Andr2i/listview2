package com.andr2i.listview.presentation.fragment.user_card;

import com.andr2i.listview.presentation.base.BaseFragmentContract;
import com.andr2i.listview.presentation.base.BasePresenter;


public interface ContactCardContract {

    interface View extends BaseFragmentContract {

    }

    interface Presenter extends BasePresenter<View> {

    }
}
