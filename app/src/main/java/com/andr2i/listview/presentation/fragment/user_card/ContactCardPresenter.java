package com.andr2i.listview.presentation.fragment.user_card;



import com.andr2i.listview.domain.InteractorMain;
import com.andr2i.listview.domain.InteractorMainContract;


public class ContactCardPresenter implements ContactCardContract.Presenter {

    private InteractorMainContract interactor;
    private ContactCardContract.View view;

    public ContactCardPresenter() {
        interactor = new InteractorMain();
    }

    @Override
    public void startView(ContactCardContract.View view) {
        this.view = view;
    }

    @Override
    public void init() {

    }

//    @Override
//    public Bitmap getQrCode(String text) {
//        try {
//            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
//            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200);
//            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
//            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            return bitmap;
//        } catch (Exception e) {
//            return null;
//        }
//    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }


}
