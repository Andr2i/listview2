package com.andr2i.listview.presentation.activity.main;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.andr2i.listview.R;
import com.andr2i.listview.databinding.ActivityMainBinding;
import com.andr2i.listview.presentation.Route;
import com.andr2i.listview.presentation.base.BaseActivity;
import com.andr2i.listview.presentation.base.BasePresenter;


public class MainActivity extends BaseActivity<ActivityMainBinding> implements IPresenter.View {

    private IPresenter.Listener presenter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new Presenter();
        Route.getInstance().startView(this);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
        presenter.init();
    }

    @Override
    protected void onDestroyView() {
        presenter.detachView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public <T> void setData(T data) {

    }

    @Override
    public <T> void transactionActivity(Class<?> activity, T object) {
        super.transactionActivity(activity, true, object);
    }

    @Override
    public void transactionFragmentDialog(DialogFragment fragment) {
        super.transactionFragmentDialog(fragment);
    }

    @Override
    public void transactionActivity(Class<?> activity) {
        super.transactionActivity(activity, true);
    }

    @Override
    public void transitionFragment(Fragment fragment) {
        super.transactionFragmentWithBackStack(fragment);
    }

    @Override
    public void transitionFragmentDialog(DialogFragment fragment) {
        super.transactionFragmentDialog(fragment);
    }


}

