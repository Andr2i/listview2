package com.andr2i.listview.presentation.fragment.user_list;


import com.andr2i.listview.data.model.db.User;
import com.andr2i.listview.presentation.base.BaseFragmentContract;
import com.andr2i.listview.presentation.base.BasePresenter;
import com.andr2i.utils.recyclerview.BaseRecyclerListener;


import java.util.List;

public interface ListContactContract {

    interface View extends BaseRecyclerListener, BaseFragmentContract {
        void initAdapter(List<User> list);

        void onShowContactCardClick(int position);

        void onUpdateClick(int position);

        void onDeleteClick(int position);

    }

    interface Presenter extends BasePresenter<View> {

        void editContact(User user);

        void deleteContact(User user);

        void addContact();

        void showContactCard(User user);
    }

}
